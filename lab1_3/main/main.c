#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "esp_log.h"
#include "sdkconfig.h"
#include "esp_system.h"
#include "esp_event.h"

#define Blink_Gpio 7

void blink_task (void *pvParameter){
	esp_rom_gpio_pad_select_gpio(Blink_Gpio);
	gpio_set_direction(Blink_Gpio, GPIO_MODE_OUTPUT);

	while(1){
		gpio_set_level(Blink_Gpio, 1);
		vTaskDelay(1000 / portTICK_PERIOD_MS);
		gpio_set_level(Blink_Gpio, 0);
		vTaskDelay(1000 / portTICK_PERIOD_MS);
	}
}

void app_main(){
	xTaskCreate(&blink_task, "blink_task", 2048, NULL, 5, NULL);
}
