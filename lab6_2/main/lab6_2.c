#include <stdio.h>
#include <string.h>
#include "driver/gpio.h"
#include "driver/adc.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_log.h"
#include "nvs_flash.h"

#define ADC_CHANNEL       ADC_CHANNEL_0  // ADC channel for photodiode input
#define THRESHOLD         120           // ADC threshold for detecting high intensity
#define DOT_DURATION      100            // Duration in milliseconds for a dot
#define DASH_DURATION     325            // Duration in milliseconds for a dash
#define SPACE_DURATION    410            // Duration in milliseconds for a space

static bool is_receiving = false;
static int start_time = 0;
static int dot_count = 0;
static int space_count = 0;
static char morse_code_buffer[256];
static int morse_code_index = 0;

void add_morse_code(char morse_code)
{
    morse_code_buffer[morse_code_index++] = morse_code;
}

void translate_morse_code()
{
    // Morse code translation logic
    static const char* morse_code_letters[] = {
        ".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..",
        "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-",
        "-.--", "--.."
    };

    static const char* morse_code_numbers[] = {
        "-----", ".----", "..---", "...--", "....-", ".....", "-....", "--...", "---..", "----."
    };

    // Convert the morse code buffer to a null-terminated string
    morse_code_buffer[morse_code_index] = '\0';

    // Find the corresponding English letter or number
    char character = ' ';
    for (int i = 0; i < 26; i++) {
        if (strcmp(morse_code_letters[i], morse_code_buffer) == 0) {
            character = 'A' + i;
            break;
        }
    }
    if (character == ' ') {
        for (int i = 0; i < 10; i++) {
            if (strcmp(morse_code_numbers[i], morse_code_buffer) == 0) {
                character = '0' + i;
                break;
            }
        }
    }

    // Print the character
    printf("%c ", character);

    // Clear the morse_code_buffer and reset the index
    memset(morse_code_buffer, 0, sizeof(morse_code_buffer));
    morse_code_index = 0;
}

void app_main(void)
{
    // Configure ADC
    adc1_config_width(ADC_WIDTH_BIT_12);
    adc1_config_channel_atten(ADC_CHANNEL, ADC_ATTEN_DB_11);

    while (1)
    {
        // Read ADC value
        int adc_value = adc1_get_raw(ADC_CHANNEL);
        if (adc_value > THRESHOLD)
        {
            if (!is_receiving)
            {
                // Start of a new Morse code symbol
                start_time = xTaskGetTickCount();
                is_receiving = true;
                dot_count = 0;
                space_count = 0;
            }
            else
            {
                // Increment dot count if already receiving
                dot_count++;
                space_count = 0;
            }
        }
        else if (is_receiving)
        {
            // End of a Morse code symbol
            int elapsed_time = (xTaskGetTickCount() - start_time) * portTICK_PERIOD_MS;

            if (elapsed_time >= DASH_DURATION)
            {
                // Detected a dash
                add_morse_code('-');
            }
	    else if (elapsed_time >= DOT_DURATION)
            {
                // Detected a dot
                add_morse_code('.');
            }

            // Reset flag and dot count
            is_receiving = false;
            dot_count = 0;

            // Check if end of word
            if (space_count >= SPACE_DURATION / 10)
            {
                // Detected a space
		printf("Space1\n");
                translate_morse_code();
                space_count = 0;
            }
        }
        else
        {
            // Increment space count if not receiving
            space_count++;
	    if (space_count >= SPACE_DURATION / 10){
		    translate_morse_code();
		    space_count = 0;
            }
	}

        // Delay to control the sampling rate
        vTaskDelay(pdMS_TO_TICKS(10));
    }
}
